"""
randomAgent.py

A simple agent that performs random actions at each time step.  Used to test
out integrating agents into the tsfServer code for players to play against
through a browser.
"""

import pyTSF as tsf
import random

class RandomAgent(tsf.CallbackAgent):
   """
   An agent that performs random actions
   """

   def __init__(self, player):
      """
      Create a new random player

      player - instance of the player to control
      """

      # Needed to allow this object
      # to be used as a CallbackAgent in the underlying TSF game
      tsf.CallbackAgent.__init__(self)

      self.player = player
      self.hasStateChanged = False     # Avoid a premature call to update

   def update(self):
      """
      Called periodically by the game
      """

      # Don't do anything if the state has not changed
      if not self.hasStateChanged:
         return

      # Otherwise, create a random command
      command = tsf.PlayerCommand()
      command.thrust = random.random() > 0.5  # True 50% of the time
      command.turn = random.choice([tsf.NO_TURN, tsf.TURN_LEFT, tsf.TURN_RIGHT])
      self.player.command(command)
      
      # Indicate that the state has been processed
      self.hasStateChanged = False

   def onReceiveGameState(self):
      """
      Called when the game has sent an updated game state
      """

      self.hasStateChanged = True 

