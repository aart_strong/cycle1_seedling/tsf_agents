from .hatTSF import HatTSF
import numpy as np
import time
from .policies import policies

class Turn_Only_Shooter(HatTSF):
	''' turn - NN, input: polar coordinate
		thrust - rule-based limited to 25
		fire - rule-based
		Algo Author: Yikang, wrapped by Tianwei
	'''
	def __init__(self, player, id=1, speed=35, drag=0.05):
		super().__init__(player, id, drag)
		self.model = policies["turn_only_shooter_%d" % speed]
		# if speed == 25:
		# 	self.model = DQN.load(self.model_path + 'shooter25_71.zip')
		# elif speed == 35:
		# 	self.model = DQN.load(self.model_path + 'shooter35_1554.zip')
		# elif speed == 45:
		# 	self.model = DQN.load(self.model_path + 'shooter45_1412.zip')

	def update(self):
		if not self.hasStateChanged:
			return
		
		state = self.state_queue[-1].copy() # rightmost/current state
		agent_state = state[self.id*6 + 4: self.id*6 + 10].copy()
		human_state = state[(1-self.id)*6 + 4: (1-self.id)*6 + 10]

		if state[1] and human_state[-1] and state[2] != self.id: # both alive and not be target
			if state[2] == 1 - self.id or np.linalg.norm(agent_state[:2]) > 220: # radius
				turn, _ = self.model.predict(self.coordinate_transform(agent_state[0], agent_state[1], agent_state[2], state[0]))
				thrust = self.rule_based_thrust(agent_state[3:5])
				fire = self.rule_based_missile(state, agent_state)
			else: # no target and very close to region: wait
				turn, thrust, fire = 0, 0, 0
		elif self.inRegion(agent_state[:2], r=220):
			fire = 0
			turn, thrust = self.leave_out(agent_state)
		else:
			turn, thrust, fire = 0, 0, 0

		action_array = np.array([turn, thrust, fire])
		self.emit_command(action_array)
		

	def rule_based_thrust(self, velo, max_speed=50):
		speed = np.linalg.norm(velo)
		thrust = 1 if speed < max_speed else 0
		return thrust
	
	def coordinate_transform(self, x, y, shooter_angle, fortress_theta):
		fortress_theta = fortress_theta / 180 * np.pi
		shooter_angle = shooter_angle / 180 * np.pi
		shooter_r = np.sqrt(x**2 + y**2)
		if x > 0:
			shooter_theta = np.arctan(y / x)
			if y < 0:
				shooter_theta += np.pi * 2
		elif x < 0:
			shooter_theta = np.arctan(y / x) + np.pi
		else:
			if y >= 0:
				shooter_theta = np.pi / 2
			else:
				shooter_theta = np.pi * 3 / 2
		shooter_theta_tran = (shooter_theta - fortress_theta) % (2 * np.pi)
		x_tran = np.cos(shooter_theta_tran) * shooter_r
		y_tran = np.sin(shooter_theta_tran) * shooter_r
		shooter_angle_tran = (shooter_angle - fortress_theta) % (2 * np.pi)
		return np.array([x_tran, y_tran, shooter_angle_tran / np.pi * 180])  

	def get_log_prob(self, state, action, turn_only=False):
		agent_state = state[self.id*6 + 4: self.id*6 + 10].copy()
		human_state = state[(1-self.id)*6 + 4: (1-self.id)*6 + 10]

		if state[1] and human_state[-1] and state[2] != self.id: # both alive and not be target
			if state[2] == 1 - self.id or np.linalg.norm(agent_state[:2]) > 220: # radius
				shooter_state = self.coordinate_transform(agent_state[0], agent_state[1], agent_state[2], state[0])
				turn_prob = np.log(self.model.action_probability(shooter_state, actions=action[0])+self.eps) # turn
				thrust = self.rule_based_thrust(agent_state[3:5])
				return turn_prob + (1.0 - (thrust == action[1])) * np.log(self.eps)
			else: # no target and very close to region: wait
				turn, thrust, fire = 0, 0, 0
		elif self.inRegion(agent_state[:2], r=220):
			fire = 0
			turn, thrust = self.leave_out(agent_state)
		else:
			turn, thrust, fire = 0, 0, 0
		return (1.0 - (turn == action[0])) * np.log(self.eps) + (1.0 - (thrust == action[1])) * np.log(self.eps)

	def get_action_dist(self, state):
		# Shooter
		# action space: turn {3} x thrust {2} x fire {2}
		action_dist = np.zeros((3, 2, 2))

		for turn in range(3):
			for thrust in range(2):
				for fire in range(2):
					action_dist[turn, thrust, fire] = self.get_log_prob(state, np.array([turn, thrust, fire]))

		return action_dist
