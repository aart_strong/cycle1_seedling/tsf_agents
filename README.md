# TSF HAT Agents

## Installation
- Python 3.6+
- OpenAI gym
- PyTorch 1.4
- TensorFlow 1.14
- [stable-baselines](https://stable-baselines.readthedocs.io/en/master/guide/install.html)

```shell
conda install pytorch torchvision -c pytorch # 1.4+
# or pip install torch==1.5.0+cu101 torchvision==0.6.0+cu101 -f https://download.pytorch.org/whl/torch_stable.html
pip install tensorflow-gpu==1.14.0  # gpu version if available
pip install stable-baselines[mpi] # for mpi, maybe stable-baseline3 later
pip install gym
```

## RL agents
- `bait_yikang.py`: 7 types by `bait_type`
- `bait_sid.py`: 2 types by `bait_type`
- `shooter_mirror.py`: specify `attack_dist`
- `shooter_turn_only.py`: 3 types by `speed`

## Adaptive agents
- `adaptive_agent.py`

## Policy Submodules
- Policy Representation (Sid)
- Policy Similarity (Tianwei)
  - Also contains `utils.py` for parse Json to numpy array
