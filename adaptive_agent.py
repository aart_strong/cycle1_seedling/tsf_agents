import pyTSF as tsf
import agents
import random as rd
import os
from numpy import array
import numpy as np
from .hatTSF import HatTSF
from .bc import BC
from collections import deque
import json


class BCAgent(HatTSF):
	def __init__(self, player, id=1, role='B', window_size=400):
		super().__init__(player, id)
		self.bc = BC(player, human_id=1-self.id) # human_id=0
		self.policies = {key: (agent_class[0](player), agent_class[1])
							for key, agent_class in agents.repo.items() 
							if key in list(self.agent_repo(role).keys())}
		if role == 'B':
			self.current_policy, self.current_role = self.policies["bait_sid_1"]
			self.bait_id = id
		else:
			self.current_policy, self.current_role = self.policies["turn_only_shooter_35"]
			self.bait_id = 1-id

		self.lgpq = deque(maxlen=window_size)

		self.path = os.path.dirname(os.path.abspath(__file__))
		self.kf = np.load(os.path.join(self.path, "perf", "fortress_destroy.npy")) # assert same order
		self.kb = np.load(os.path.join(self.path, "perf", "bait_death.npy"))
		assert self.kf.shape == (len(self.bc.bait_repo), len(self.bc.shooter_repo))
		self.tick = 0
		
	def onReceiveGameState(self):
		super().onReceiveGameState()
		self.tick += 1
		# use BC to infer policy similarity
		if self.tick > 1 and self.valid_state: # ensure state_queue len == 2
			log_prob = self.bc.get_log_probs(self.human_role, 
										 self.state_queue[0], 
										 self.action_queue[0], 
										 turn_only=False) # last state and action
			self.lgpq.append(log_prob)
			current_lgpq = [lgp for lgp in self.lgpq]
			current_lgpq_avg = np.array(current_lgpq).mean(0)
			current_agent_name = self.infer(current_lgpq_avg)
			self.current_policy, self.current_role = self.policies[current_agent_name]
		else:
			print("invalid state")
		# update current policy's state and action
		self.current_policy.hasStateChanged = True
		self.current_policy.state_queue = self.state_queue # len=2
		self.current_policy.action_queue = self.action_queue # len=1

	def update(self):
		self.current_policy.update()

	def infer(self,log_prob):
		similar_agent_id = np.argmax(log_prob)
		similar_agent_name = list(self.agent_repo(self.human_role).keys())[similar_agent_id]
		print(log_prob, similar_agent_name)

		# select optimal teammate
		if self.human_role == "B":
			optimal_mate_id = np.argmax(self.kf[similar_agent_id, :])
			optimal_mate_name = list(self.agent_repo("S").keys())[optimal_mate_id]
		else:
			optimal_mate_id = np.argmax(self.kf[:, similar_agent_id])
			optimal_mate_name = list(self.agent_repo("B").keys())[optimal_mate_id]
		
		print(optimal_mate_name)
		return optimal_mate_name
	
	@property
	def valid_state(self):
		state = self.state_queue[-1].copy() # rightmost/current state
		fortress_alive, fortress_target = state[1], state[2]
		return fortress_alive == True and fortress_target == self.bait_id

	@property
	def human_role(self):
		if self.current_role == agents.Role.BAIT:
			return "S"
		return "B"

	def agent_repo(self, role):
		if role == "B":
			return self.bc.bait_repo
		return self.bc.shooter_repo
