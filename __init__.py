"""
This module contains agents that can be used as team mates in TSF.
"""

# This should preload all policy models
from .policies import policies

# Naive Agents
from .randomAgent import RandomAgent
from .zero_agent import Zero_Agent

# RL Agents
from .bait_sid import Bait_Sid
from .bait_yikang import Bait_Yikang
from .shooter_mirror import Mirror_Shooter
from .shooter_turn_only import Turn_Only_Shooter

# Adaptive Agents
from .adaptive_agent import BCAgent


import enum

class Role(enum.Enum):
	"""
	Role defines which roles an agent can take.  At the moment, an
	agent may take the role of bait (Role.BAIT), shooter (Role.SHOOTER), or 
	both (Role.ANY)
	"""

	BAIT = 1 
	SHOOTER = 2
	ANY = BAIT | SHOOTER 


# Agent repo is a dictionary mapping agent names (by string) to constructors
# for agents.  NOTE:  These shouldn't be instantiations of specific agents, in
# order to avoid sharing agents between multiple connections, and to minimize
# the amount of overhead needed.
# Each entry maps a string agent name to a tuple consisting of the constructor
# and a Role.  The Role (at the moment) should only be considered as a hint, as
# there is currently nothing in the engine enforcing roles.
repo = { 
	"random": (RandomAgent, Role.ANY),
	"zero_shooter": (Zero_Agent, Role.SHOOTER),
	"bait_sid_1": (lambda player: Bait_Sid(player, bait_type=1), Role.BAIT),
	"bait_sid_2": (lambda player: Bait_Sid(player, bait_type=2), Role.BAIT),
	"bait_yikang_1": (lambda player: Bait_Yikang(player, bait_type=1), Role.BAIT),
	"bait_yikang_2": (lambda player: Bait_Yikang(player, bait_type=2), Role.BAIT),
	"bait_yikang_3": (lambda player: Bait_Yikang(player, bait_type=3), Role.BAIT),
	"bait_yikang_4": (lambda player: Bait_Yikang(player, bait_type=4), Role.BAIT),
	"bait_yikang_5": (lambda player: Bait_Yikang(player, bait_type=5), Role.BAIT),
	"bait_yikang_6": (lambda player: Bait_Yikang(player, bait_type=6), Role.BAIT),
	"bait_yikang_7": (lambda player: Bait_Yikang(player, bait_type=7), Role.BAIT),
	"mirror_shooter_15": (lambda player: Mirror_Shooter(player, attack_dist=15), Role.SHOOTER),
	"mirror_shooter_20": (lambda player: Mirror_Shooter(player, attack_dist=20), Role.SHOOTER),
	"mirror_shooter_25": (lambda player: Mirror_Shooter(player, attack_dist=25), Role.SHOOTER),
	"mirror_shooter_30": (lambda player: Mirror_Shooter(player, attack_dist=30), Role.SHOOTER),
	"turn_only_shooter_25": (lambda player: Turn_Only_Shooter(player, speed=25), Role.SHOOTER),
	"turn_only_shooter_35": (lambda player: Turn_Only_Shooter(player, speed=35), Role.SHOOTER),
	"turn_only_shooter_45": (lambda player: Turn_Only_Shooter(player, speed=45), Role.SHOOTER),
	"bait_BCAgent_150": (lambda player: BCAgent(player, role='B', window_size=150), Role.BAIT),
	"bait_BCAgent_400": (lambda player: BCAgent(player, role='B', window_size=400), Role.BAIT),
	"bait_BCAgent_800": (lambda player: BCAgent(player, role='B', window_size=800), Role.BAIT),
	"shooter_BCAgent_150": (lambda player: BCAgent(player, role='S', window_size=150), Role.SHOOTER),
	"shooter_BCAgent_400": (lambda player: BCAgent(player, role='S', window_size=400), Role.SHOOTER),
	"shooter_BCAgent_800": (lambda player: BCAgent(player, role='S', window_size=800), Role.SHOOTER),
}
