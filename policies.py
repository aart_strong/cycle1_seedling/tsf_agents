from stable_baselines import PPO1, PPO2, A2C, TRPO, DQN
model_path = 'agents/models/'

# all agent policies with model
policies = { "bait_sid_1": PPO1.load(model_path + 'bait1_drag0.05_100000.zip'),
	         "bait_sid_2": PPO1.load(model_path + 'bait2_drag0.05_100000.zip'),
	         "bait_yikang_1": A2C.load(model_path + 'yig_best_model1747.zip'),
	         "bait_yikang_2": A2C.load(model_path + 'yig_best_model1717.zip'),
	         "bait_yikang_3": A2C.load(model_path + 'yig_bait25_1785.zip'), 
	         "bait_yikang_4": A2C.load(model_path + 'yig_bait25t_1717.zip'),
	         "bait_yikang_5": PPO2.load(model_path + 'yig_bait25tm5_ppo_1778.zip'),
	         "bait_yikang_6": TRPO.load(model_path + 'yig_bait25tm5_trpo_1797.zip'),
	         "bait_yikang_7": TRPO.load(model_path + 'yig_bait_perceiveall_trpo_2188.zip'),
	         "turn_only_shooter_25": DQN.load(model_path + 'shooter25_71.zip'),
	         "turn_only_shooter_35": DQN.load(model_path + 'shooter35_1554.zip'),
	         "turn_only_shooter_45": DQN.load(model_path + 'shooter45_1412.zip'),
           }