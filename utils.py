import numpy as np
import json
from heapq import heappop, heappush, heappushpop

def getStateDict(gameState):
	"""
	Assign tsf.gameState to the state_dict
	Game state_dict is a dictionary with the following schema:
	'players' -	list of players with following schema
		[id, x, y, vx, vy, angle, alive]
	'fortresses' - list of fortresses with following schema
		[id, x, y, angle, alive, vulnerable, target, shield_radius, activation_region_radius]
	'shells' - list of shells with theh following schema
		[id, x, y, angle]
	'missiles' - list of missiles with the following schema
		[id, x, y, angle]
	'border' - [xmin, xmax, ymin, ymax]
	'width' - width of game region
	'height' - height of game region
	'score' - current score
	"""
	border = gameState.borderState
	state_dict = {'players': [],
			   'fortresses': [],
			   'shells': [],
			   'missiles': [],
			   'border': [border.xMin, border.xMax, border.yMin, border.yMax],
			   'width': gameState.width,
			   'height': gameState.height,
			   'score': gameState.score,
			   'actions': [],
			   'messages': []
			  }
	
	# Extract player data
	for player in gameState.players:
		state_dict['players'].append((player.id, player.position.mX, player.position.mY, player.velocity.mX, player.velocity.mY, player.angle, player.alive))
		state_dict['actions'].append([player.action.turn, int(player.action.thrust), int(player.action.fire)]) # must parse it
	
	for fortress in gameState.fortresses:
		state_dict['fortresses'].append((fortress.id, fortress.position.mX, fortress.position.mY, fortress.angle, fortress.alive, fortress.shieldState.vulnerable, fortress.target, fortress.shieldState.radius, fortress.activationRegionState.radius))
	
	for shell in gameState.shells:
		state_dict['shells'].append((shell.id, shell.position.mX, shell.position.mY, shell.angle))
	for missile in gameState.missiles:
		state_dict['missiles'].append((missile.id, missile.position.mX, missile.position.mY, missile.angle))
	for message in gameState.messages:
		state_dict['messages'].append(message)
	return state_dict

def getStateArray(state_dict, bait_id: int, shells: bool):
	'''
	currently follow `make_dataset.py`
	size: (28,)
	05 angle                           # 00 keep
	06 alive                           # 01 keep
	07 target_id                       # 02 keep (-1, 0, 1)
	08 vulnerable                      # 03 keep
	17 x                               # 04 keep player id 0 (in human data, Bait)
	18 y                               # 05
	19 angle                           # 06 
	20 vx                              # 07 
	21 vy                              # 08
	22 alive                           # 09 
	23 x                               # 10 keep player id 1 (in human data, Shooter)
	24 y                               # 11
	25 angle                           # 12
	26 vx                              # 13 
	27 vy                              # 14 
	28 alive                           # 15 
	shells: (?, 5)
	2 x                                # 16, 19
	3 y                                # 17, 20   
	4 angle                            # 18, 21
	missiles: (?, 5)
	2 x                                # 22, 25
	3 y                                # 23, 26   
	4 angle                            # 24, 27
	'''
	state_array = -1 * np.ones((28,), np.float)
	state_array[:4] = np.array([
		state_dict['fortresses'][0][3], 
		state_dict['fortresses'][0][4], 
		state_dict['fortresses'][0][6],
		state_dict['fortresses'][0][5]
	])
	state_array[4:16] = np.array([
		state_dict['players'][0][1],
		state_dict['players'][0][2],
		state_dict['players'][0][5],
		state_dict['players'][0][3],
		state_dict['players'][0][4],
		state_dict['players'][0][6],
		state_dict['players'][1][1],
		state_dict['players'][1][2],
		state_dict['players'][1][5],
		state_dict['players'][1][3],
		state_dict['players'][1][4],
		state_dict['players'][1][6],
	])

	if shells: # default: Yikang bait, two nearest shells to the bait
		bait_pos = state_array[bait_id*6 + 4: bait_id*6 + 6]
		heap = []
		for (_, s_x, s_y, s_a) in state_dict['shells']:
			if len(heap) < 2:
				heappush(heap, (-dist_from_player(bait_pos, np.array([s_x, s_y])), s_x, s_y, s_a))
			else:
				heappushpop(heap, (-dist_from_player(bait_pos, np.array([s_x, s_y])), s_x, s_y, s_a))
		if heap:
			state_array[16:19] = heappop(heap)[1:4]
		else:
			state_array[16:19] = [0, 1, 90]
		if heap:
			state_array[19:22] = heappop(heap)[1:4]
		else:
			state_array[19:22] = [0, 1, 90]
	
	else:	# nearest shell to bait and shooter
		for (_, s_x, s_y, s_a) in state_dict['shells']:
			if state_array[18] == -1 or \
				dist_from_player(state_array[4:6], np.array([s_x, s_y])) < dist_from_player(state_array[4:6], state_array[16:18]):
				state_array[16:19] = np.array([s_x, s_y, s_a])
			if state_array[21] == -1 or \
				dist_from_player(state_array[10:12], np.array([s_x, s_y])) < dist_from_player(state_array[10:12], state_array[19:21]):
				state_array[19:22] = np.array([s_x, s_y, s_a])
	
	for (_, s_x, s_y, s_a) in state_dict['missiles']:
		if state_array[24] == -1 or \
			dist_from_player(state_array[4:6], np.array([s_x, s_y])) < dist_from_player(state_array[4:6], state_array[22:24]):
			state_array[22:25] = np.array([s_x, s_y, s_a])
		if state_array[27] == -1 or \
			dist_from_player(state_array[10:12], np.array([s_x, s_y])) < dist_from_player(state_array[10:12], state_array[25:27]):
			state_array[25:28] = np.array([s_x, s_y, s_a])
	
	action_array = np.concatenate(state_dict['actions'])
	return state_array, action_array

def dist_from_player(player_pos, projectile_pos):
	return np.linalg.norm(player_pos - projectile_pos)
