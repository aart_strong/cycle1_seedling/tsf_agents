# Tianwei: behavior cloning method
import os
import numpy as np
import time

# RL Agents
from .bait_sid import Bait_Sid
from .bait_yikang import Bait_Yikang
from .shooter_mirror import Mirror_Shooter
from .shooter_turn_only import Turn_Only_Shooter


class BC:
	def __init__(self, player, human_id=0):
		# if the human were a agent
		assert human_id in [0, 1]
		self.human_id = human_id
		# python 3.6+ maintains insertion order

		self.bait_repo = {
			"bait_yikang_1": Bait_Yikang(player, human_id, bait_type=1),
			"bait_yikang_2": Bait_Yikang(player, human_id, bait_type=2),
			"bait_yikang_3": Bait_Yikang(player, human_id, bait_type=3),
			"bait_yikang_4": Bait_Yikang(player, human_id, bait_type=4),
			"bait_yikang_5": Bait_Yikang(player, human_id, bait_type=5),
			"bait_yikang_6": Bait_Yikang(player, human_id, bait_type=6),
			"bait_yikang_7": Bait_Yikang(player, human_id, bait_type=7),
			"bait_sid_1": Bait_Sid(player, human_id, bait_type=1),
			"bait_sid_2": Bait_Sid(player, human_id, bait_type=2),
		}

		self.shooter_repo = {
			"mirror_shooter_15": Mirror_Shooter(player, human_id, attack_dist=15),
			"mirror_shooter_20": Mirror_Shooter(player, human_id, attack_dist=20),
			"mirror_shooter_25": Mirror_Shooter(player, human_id, attack_dist=25),
			"mirror_shooter_30": Mirror_Shooter(player, human_id, attack_dist=30),
			"turn_only_shooter_25": Turn_Only_Shooter(player, human_id, speed=25),
			"turn_only_shooter_35": Turn_Only_Shooter(player, human_id, speed=35),
			"turn_only_shooter_45": Turn_Only_Shooter(player, human_id, speed=45),
		}

		self.reset()

	def reset(self, query: str = "Both"):
		assert query in ["B", "S", "Both"]
		if query in ["B", "Both"]:
			self.bait_logprobs = []
		if query in ["S", "Both"]:
			self.shooter_logprobs = []

	def get_log_probs(self, query: str, state, actions, turn_only=False):
		''' params:
				- query_bait: "B" if human is bait, else "S"
				- state: np.array(28) converted from util/
				- actions: np.array(6) total actions
				Note: (state, actions) must be at the same timestamp (st, at+1) in our case
				min: 2*log(1e-8) = -36.84
		'''
		assert query in ["B", "S"]
		if turn_only: assert query == "B"

		if self.human_id == 0:
			action = actions[:3].astype(np.int)
		else:
			action = actions[3:].astype(np.int)

		if query == "B":
			agent_repo = self.bait_repo
			log_probs = self.bait_logprobs
		else:
			agent_repo = self.shooter_repo
			log_probs = self.shooter_logprobs

		log_prob = np.zeros((len(agent_repo)))
		for idx, (key, model) in enumerate(agent_repo.items()):
			# print(key, model)
			log_prob[idx] = model.get_log_prob(state, action, turn_only)
		log_probs.append(log_prob)
		return log_prob
	
	def summary(self, query: str):
		''' return current log_probs statistics
		'''
		assert query in ["B", "S"]
		if query == "B":
			log_probs = self.bait_logprobs
		else:
			log_probs = self.shooter_logprobs
		return np.vstack(log_probs)
		
