from .hatTSF import HatTSF
import numpy as np
import time
from .policies import policies

class Bait_Sid(HatTSF):
	''' 
		Algo Author: Sid
	'''
	def __init__(self, player, id=1, drag=0.05, bait_type=1):
		super().__init__(player, id, drag)
		assert bait_type in [1, 2]
		self.model = policies['bait_sid_%d' % bait_type]
#		self.model = PPO1.load(self.model_path + 'bait%d_drag0.05_100000.zip' % bait_type)
		
	def update(self):
		if not self.hasStateChanged:
			return
		
		state = self.state_queue[-1].copy() # rightmost/current state
		agent_state = state[self.id*6 + 4: self.id*6 + 10].copy()
		human_state = state[(1-self.id)*6 + 4: (1-self.id)*6 + 10]

		if state[1] and human_state[-1]: # alive
			# if not in region first go inside and do stuff.
			if not self.inRegion(agent_state[:2], r=200): # out of region
				turn, thrust = self.enter_in(agent_state)

			# only use learning based methods when the bait is inside the activation region.
			else: # inside. learning-based

				bait_state = np.zeros(16)
				bait_state[:4] = state[:4] # fortress
				bait_state[4:10] = agent_state # bait
				bait_state[10:13] = state[16:19] # shell
				bait_state[13:16] = state[22:25] # missile

				action = self.model.predict(bait_state)
				turn, thrust = action[0][0], action[0][1]

		# if fortress or the bait is dead move out.
		else: # dead
			if self.inRegion(agent_state[:2], r=220): # inside region
				turn, thrust = self.leave_out(agent_state)
			else: # outside
				turn, thrust = 0, 0

		action_array = np.array([turn, thrust, 0])
		self.emit_command(action_array)

	def get_log_prob(self, state, action, turn_only=False):
		agent_state = state[self.id*6 + 4: self.id*6 + 10].copy()
		human_state = state[(1-self.id)*6 + 4: (1-self.id)*6 + 10]

		if state[1] and human_state[-1]: # alive
			# if not in region first go inside and do stuff.
			if not self.inRegion(agent_state[:2], r=200): # out of region
				turn, thrust = self.enter_in(agent_state)
				log_prob = (1.0 - (turn == action[0])) * np.log(self.eps) + (1.0 - (thrust == action[1])) * np.log(self.eps)

			# only use learning based methods when the bait is inside the activation region.
			else: # inside. learning-based
				bait_state = np.zeros(16)
				bait_state[:4] = state[:4] # fortress
				bait_state[4:10] = agent_state # bait
				bait_state[10:13] = state[16:19] # shell
				bait_state[13:16] = state[22:25] # missile

				log_prob = np.log(self.model.action_probability(bait_state, actions=action)+self.eps) # turn, thrust, fire?
		# if fortress or the bait is dead move out.
		else: # dead
			if self.inRegion(agent_state[:2], r=220): # inside region
				turn, thrust = self.leave_out(agent_state)
			else: # outside
				turn, thrust = 0, 0
		
			log_prob =  (1.0 - (turn == action[0])) * np.log(self.eps) + (1.0 - (thrust == action[1])) * np.log(self.eps)
		
		if turn_only:
			return log_prob / 2.0
		else:
			return log_prob
	
	def get_action_dist(self, state):
		# action space: turn {3} x thrust {2}
		action_dist = np.zeros((3, 2))

		for turn in range(3):
			for thrust in range(2):
				action_dist[turn, thrust] = self.get_log_prob(state, np.array([turn, thrust]))

		return action_dist