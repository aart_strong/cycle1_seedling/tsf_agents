from .hatTSF import HatTSF
import numpy as np
import time
from heapq import heapify, heappop, heappush, heappushpop, heapreplace
from .policies import policies

class Bait_Yikang(HatTSF):
	''' 
		Algo Author: Yikang, wrapped by Tianwei
	'''
	def __init__(self, player, id=1, bait_type=3, drag=0.05):
		super().__init__(player, id, drag)
		assert bait_type in range(1, 8)
		self.bait_min_speed = 5
		self.thrust = False
		self.perceive_shooter = False
		self.min_speed = False
		self.model = policies['bait_yikang_%d' % bait_type]

		if bait_type == 1:
			self.max_speed = 5
			# self.model = A2C.load(self.model_path + 'yig_best_model1747.zip')
		if bait_type == 2:
			self.max_speed = 15
			# self.model = A2C.load(self.model_path + 'yig_best_model1717.zip')
		if bait_type == 3:
			self.max_speed = 25
			# self.model = A2C.load(self.model_path + 'yig_bait25_1785.zip')
		if bait_type == 4:
			self.max_speed = 25
			self.thrust = True
			# self.model = A2C.load(self.model_path + 'yig_bait25t_1717.zip')
		if bait_type == 5:
			self.max_speed = 25
			self.thrust = True
			# self.model = PPO2.load(self.model_path + 'yig_bait25tm5_ppo_1778.zip')
			self.min_speed = True
		if bait_type == 6:
			self.max_speed = 25
			self.thrust = True
			# self.model = TRPO.load(self.model_path + 'yig_bait25tm5_trpo_1797.zip')
			self.min_speed = True
		if bait_type == 7:
			self.max_speed = 25
			self.thrust = True
			self.perceive_shooter = True
			# self.model = TRPO.load(self.model_path + 'yig_bait_perceiveall_trpo_2188.zip')
			self.min_speed = True
		
	def update(self):
		if not self.hasStateChanged:
			return
		
		state = self.state_queue[-1].copy() # rightmost/current state
		agent_state = state[self.id*6 + 4: self.id*6 + 10].copy()
		human_state = state[(1-self.id)*6 + 4: (1-self.id)*6 + 10]

		if state[1] and human_state[-1]: # alive
			if not self.inRegion(agent_state[:2], r=200): # out of region
				turn, thrust = self.enter_in(agent_state)
			else: # inside. learning-based
				bait_theta, bait_r = self.to_polar(agent_state[0], agent_state[1])
				shell_1_x, shell_1_y = self.shell_to_polar(state[16], state[17], bait_theta)
				shell_2_x, shell_2_y = self.shell_to_polar(state[19], state[20], bait_theta)
				bait_state = np.array([bait_r, (agent_state[2] - bait_theta) % 360, shell_1_x, shell_1_y, shell_2_x, shell_2_y])
				if self.min_speed:
					bait_vx, bait_vy = self.shell_to_polar(agent_state[3], agent_state[4], bait_theta)
					bait_state = np.concatenate([bait_state, [bait_vx, bait_vy]])
				if self.perceive_shooter:
					shooter_x, shooter_y = self.shell_to_polar(human_state[0], human_state[1], bait_theta)
					shooter_vx, shooter_vy = self.shell_to_polar(human_state[3], human_state[4], bait_theta)
					bait_state = np.concatenate([bait_state, [shooter_x, shooter_y, shooter_vx, shooter_vy]]) # perceive shooter pos and velo
				if self.thrust:
					(turn, thrust), _ = self.model.predict(bait_state)
					if self.min_speed: # and velo state
						thrust = self.min_thrust(agent_state[3:5], thrust)
				else:
					turn, _ = self.model.predict(bait_state)
					thrust = self.rule_based_thrust(agent_state[3:5])

		else: # dead
			if self.inRegion(agent_state[:2], r=220): # inside region
				turn, thrust = self.leave_out(agent_state)
			else: # outside
				turn, thrust = 0, 0

		action_array = np.array([turn, thrust, 0])
		self.emit_command(action_array)
		
	def rule_based_thrust(self, velo):
		speed = np.linalg.norm(velo)
		thrust = 1 if speed < self.max_speed else 0
		return thrust

	def min_thrust(self, velo, thrust):
		speed = np.linalg.norm(velo)
		thrust = 1 if speed < self.bait_min_speed else thrust
		return thrust

	def shell_to_polar(self, x, y, bait_theta):
		shell_theta, shell_r = self.to_polar(x, y)
		shell_theta_tran = ((shell_theta - bait_theta) % 360) / 180 * np.pi
		x_tran, y_tran = np.cos(shell_theta_tran) * shell_r, np.sin(shell_theta_tran) * shell_r
		return x_tran, y_tran

	def to_polar(self, x, y):
		theta = self.position_angle(x, y) / np.pi * 180
		return theta, np.sqrt(x**2 + y**2)

	def get_log_prob(self, state, action, turn_only=False):
		# whole state, its action
		agent_state = state[self.id*6 + 4: self.id*6 + 10].copy()
		human_state = state[(1-self.id)*6 + 4: (1-self.id)*6 + 10]

		if state[1] and human_state[-1]: # alive
			if not self.inRegion(agent_state[:2], r=200): # out of region
				turn, thrust = self.enter_in(agent_state)
				log_prob = (1.0 - (turn == action[0])) * np.log(self.eps) + (1.0 - (thrust == action[1])) * np.log(self.eps)
				if turn_only: log_prob /= 2.0
			else: # inside. learning-based
				# state = self.fill_shells_into_state(state, agent_state, state_dict)
				bait_theta, bait_r = self.to_polar(agent_state[0], agent_state[1])
				shell_1_x, shell_1_y = self.shell_to_polar(state[16], state[17], bait_theta)
				shell_2_x, shell_2_y = self.shell_to_polar(state[19], state[20], bait_theta)
				bait_state = np.array([bait_r, (agent_state[2] - bait_theta) % 360, shell_1_x, shell_1_y, shell_2_x, shell_2_y])
				if self.min_speed: # perceive its speed
					bait_vx, bait_vy = self.shell_to_polar(agent_state[3], agent_state[4], bait_theta)
					bait_state = np.concatenate([bait_state, [bait_vx, bait_vy]])
				if self.perceive_shooter:
					shooter_x, shooter_y = self.shell_to_polar(human_state[0], human_state[1], bait_theta)
					shooter_vx, shooter_vy = self.shell_to_polar(human_state[3], human_state[4], bait_theta)
					bait_state = np.concatenate([bait_state, [shooter_x, shooter_y, shooter_vx, shooter_vy]]) # perceive shooter pos and velo
				if self.thrust:
					log_prob = np.log(self.model.action_probability(bait_state, actions=action[:2])+self.eps) # (turn, thrust)
					if turn_only: log_prob /= 2.0
					# if self.min_speed: # and velo state
					# 	thrust = self.min_thrust(agent_state[3:5], thrust)
				else:
					turn_prob = np.log(self.model.action_probability(bait_state, actions=action[0])+self.eps) # turn
					thrust = self.rule_based_thrust(agent_state[3:5])
					if turn_only: log_prob = turn_prob
					else: log_prob = turn_prob + (1.0 - (thrust == action[1])) * np.log(self.eps)
		else: # dead
			if self.inRegion(agent_state[:2], r=220): # inside region
				turn, thrust = self.leave_out(agent_state)
			else: # outside
				turn, thrust = 0, 0

			log_prob = (1.0 - (turn == action[0])) * np.log(self.eps) + (1.0 - (thrust == action[1])) * np.log(self.eps)
			if turn_only: log_prob /= 2.0
			
		return log_prob

	def get_action_dist(self, state):
		# action space: turn {3} x thrust {2}
		action_dist = np.zeros((3, 2))

		for turn in range(3):
			for thrust in range(2):
				action_dist[turn, thrust] = self.get_log_prob(state, np.array([turn, thrust]))

		return action_dist
